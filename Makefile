up:
	sudo docker-compose up -d

migrate:
	docker-compose exec php php artisan migrate

stop:
	sudo docker-compose stop

restart:
	sudo docker-compose restart

seed:
	docker-compose exec php php artisan db:seed

rollback:
	docker-compose exec php php artisan migrate:rollback

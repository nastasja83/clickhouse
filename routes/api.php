<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserVacancyActionController;
use App\Http\Controllers\Api\AuthorizationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/auth_token', [AuthorizationController::class, 'getAuthToken'])
    ->middleware('access')
    ->name('auth.token');
Route::group([
    'prefix' => 'user_actions',
    'middleware' => 'authorize'
], function () {
    Route::get('/', [UserVacancyActionController::class, 'getVacancyActions'])
    ->name('actions');
    Route::post('/store', [UserVacancyActionController::class, 'store'])
    ->name('actions.store');
});

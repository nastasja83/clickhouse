<?php

return [

    'token' => [
        'lifetime' => 10,
        'length' => 27,
        'separator' => '/',
        'access_token' => env('ACCESS_TOKEN')
    ]
];

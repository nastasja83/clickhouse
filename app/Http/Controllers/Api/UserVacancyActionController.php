<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\UserVacancyActionService;
use Illuminate\Http\Request;
use App\Models\Analytics\UserVacancyAction;

class UserVacancyActionController extends Controller
{
    public function store(Request $request): void
    {
        $data = $request->validate(UserVacancyActionService::getCreateRules());
        UserVacancyAction::create($data);
    }

    public function getVacancyActions(Request $request)
    {
        if (!$request->actor_id) {
            abort(404);
        }
        return UserVacancyActionService::getVacancyActionsByUserId($request->actor_id);
    }
}

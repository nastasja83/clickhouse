<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\AuthService;

class AuthorizationController extends Controller
{
    public function getAuthToken()
    {
        return AuthService::createToken();
    }
}

<?php

namespace App\Models\Analytics;

use PhpClickHouseLaravel\BaseModel;

class UserVacancyAction extends BaseModel
{
    protected $table = 'user_vacancy_actions';
}

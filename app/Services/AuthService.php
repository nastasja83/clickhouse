<?php

namespace App\Services;

use Illuminate\Support\Str;
use Carbon\Carbon;

class AuthService
{
    public static function createToken(): array
    {
        $timestamp = Carbon::now()->addMinutes(config('const.token.lifetime'))->timestamp;
        $textLength = config('const.token.length') - strlen($timestamp);
        $remainder = ($textLength + strlen($timestamp . config('const.token.separator'))) % 3;

        if ($remainder != 0) {
            $textLength = $textLength - $remainder;
        }
        return [
            'token' => base64_encode(
                $timestamp
                . config('const.token.separator')
                . Str::random($textLength)
            )
        ];
    }

    public static function getTokenTimestamp(string $token): int
    {
        $token = base64_decode($token);
        return (int) mb_stristr($token, config('const.token.separator'), true);
    }
}

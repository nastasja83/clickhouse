<?php

namespace App\Services;

use App\Models\Analytics\UserVacancyAction;

class UserVacancyActionService
{
    public static function getCreateRules(): array
    {
        return [
            'created_at' => 'required|date',
            'like' => 'nullable|integer',
            'unlike' => 'nullable|integer',
            'view' => 'nullable|integer',
            'response' => 'nullable|integer',
            'vacancy_id' => 'required|integer',
            'positions' => 'nullable|array',
            'skills' => 'nullable|array',
            'duties' => 'nullable|array',
            'requirements' => 'nullable|array',
            'user_id' => 'required|integer',
        ];
    }

    public static function getVacancyActionsByUserId($id)
    {
        return UserVacancyAction::select(['*'])
            ->where('user_id', '=', $id)
            ->orderByDesc('created_at')
            ->getRows();
    }
}

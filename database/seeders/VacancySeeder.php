<?php

namespace Database\Seeders;

use App\Models\Vacancy;
use Illuminate\Database\Seeder;
use Faker;

class VacancySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for ($i = 10000; $i >= 1; $i--) {
            Vacancy::create([
                'id' => rand(1, 1000),
                'title' => $faker->jobTitle(),
                'city' => $faker->city(),
                'salary' => rand(10000, 150000),
                'is_remote' => rand(0, 1),
                'is_premium' => rand(0, 1),
                'skills' => [$faker->word(), $faker->word(), $faker->word()],
                'perks' => [$faker->word(), $faker->word(), $faker->word()],
                'duties' => [$faker->word(), $faker->word(), $faker->word()],
                'requirements' => [$faker->word(), $faker->word(), $faker->word()],
                'profarea' => $faker->word(),
                'created_at' => $faker->date(),
            ]);
        }
    }
}

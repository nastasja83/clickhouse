<?php

class CreateUserVacancyActionsTable extends \PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write('
                CREATE TABLE user_vacancy_actions (
                created_at DateTime,
                user_id UInt64,
                like Nullable(UInt8),
                unlike Nullable(UInt8),
                view Nullable(UInt8),
                response Nullable(UInt8),
                vacancy_id UInt64,
                positions Array(String),
                skills Array(String),
                duties Array(String),
                requirements Array(String)
            )
                ENGINE = MergeTree()
                PARTITION BY toYYYYMM(created_at)
                ORDER BY(user_id, created_at)'
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write('DROP TABLE user_vacancy_actions');
    }
};
